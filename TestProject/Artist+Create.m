//
//  Artist+Create.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "Artist+Create.h"
#import "Helper.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation Artist (Create)

+(Artist *)createCountryWithDict:(NSDictionary *)dict city:(City *)city
{
    Artist *newArtist = [Artist MR_createEntityInContext:ManagedObjectContext];
    newArtist.firstName = [dict[@"FirstName"] isKindOfClass:[NSNull class]] ? @"" : dict[@"FirstName"];
    newArtist.lastName = [dict[@"LastName"] isKindOfClass:[NSNull class]] ? @"" : dict[@"LastName"];
    newArtist.age = [dict[@"Age"] isKindOfClass:[NSNull class]] ? @"" : dict[@"Age"];
    newArtist.desc = [dict[@"Description"] isKindOfClass:[NSNull class]] ? @"" : dict[@"Description"];
    newArtist.phone = [dict[@"Phone"] isKindOfClass:[NSNull class]] ? @"" : dict[@"Phone"];
    newArtist.email = [dict[@"Email"] isKindOfClass:[NSNull class]] ? @"" : dict[@"Email"];
    newArtist.vider = [dict[@"Vider"] isKindOfClass:[NSNull class]] ? @"" : dict[@"Vider"];
    newArtist.unique = [dict[@"Id"] isKindOfClass:[NSNull class]] ? @"" : dict[@"Id"];
    newArtist.city = city;
    
    [ManagedObjectContext MR_saveToPersistentStoreAndWait];
    return newArtist;
}


@end
