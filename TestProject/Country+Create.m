
//
//  Country+Create.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "Country+Create.h"
#import "Helper.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation Country (Create)

+(Country *)createCountryWithDict:(NSDictionary *)dict
{
    Country *newCountry = [Country MR_createEntityInContext:ManagedObjectContext];
    newCountry.name = dict[@"Name"];
    newCountry.unique = dict[@"Id"];
    newCountry.imageLink = [dict[@"ImageLink"] isKindOfClass:[NSNull class]] ? @"" : dict[@"ImageLink"];
    
    [ManagedObjectContext MR_saveToPersistentStoreAndWait];
    return newCountry;
}

@end
