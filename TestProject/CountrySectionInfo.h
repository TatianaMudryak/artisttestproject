//
//  CountrySectionInfo.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Country.h"
#import "CountrySectionHeaderView.h"

@interface CountrySectionInfo : NSObject

@property BOOL open;
@property Country *country;
@property CountrySectionHeaderView *headerView;

@end
