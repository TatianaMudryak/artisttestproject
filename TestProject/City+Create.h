//
//  City+Create.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "City.h"

@interface City (Create)

+(City *)createCityWithDict:(NSDictionary *)dict country:(Country *)country;

@end
