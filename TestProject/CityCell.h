//
//  CountryCell.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"

@interface CityCell : UITableViewCell

@property (nonatomic, strong) City *city;
@property (weak, nonatomic) IBOutlet UIImageView *cityImageView;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;

-(void)configureCell;

@end
