//
//  ServerAPIWrapper.m
//  TestProject
//
//  Created by Oleg on 1/13/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "ServerAPIWrapper.h"

@implementation ServerAPIWrapper

+(ServerAPIWrapper *)sharedServerWrapper
{
    __strong static ServerAPIWrapper *sharedObject = nil;
    static dispatch_once_t predicate = 0;
    dispatch_once(&predicate, ^{
        sharedObject = [[ServerAPIWrapper alloc] init];
    });
    return sharedObject;
}


@end
