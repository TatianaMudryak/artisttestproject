//
//  Country+CoreDataProperties.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Country+CoreDataProperties.h"

@implementation Country (CoreDataProperties)

@dynamic imageLink;
@dynamic name;
@dynamic unique;
@dynamic city;
@dynamic imageData;

@end
