//
//  CountryViewController.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryViewController : UITableViewController

@property (nonatomic, strong) NSArray *countries;

@end
