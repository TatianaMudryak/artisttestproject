//
//  Country+CoreDataProperties.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Country.h"

NS_ASSUME_NONNULL_BEGIN

@interface Country (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *imageLink;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSDecimalNumber *unique;
@property (nullable, nonatomic, retain) NSData *imageData;
@property (nullable, nonatomic, retain) NSSet<City *> *city;

@end

@interface Country (CoreDataGeneratedAccessors)

- (void)addCityObject:(City *)value;
- (void)removeCityObject:(City *)value;
- (void)addCity:(NSSet<City *> *)values;
- (void)removeCity:(NSSet<City *> *)values;

@end

NS_ASSUME_NONNULL_END
