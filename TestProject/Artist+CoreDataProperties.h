//
//  Artist+CoreDataProperties.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Artist.h"
@class ArtistImage;

NS_ASSUME_NONNULL_BEGIN

@interface Artist (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *age;
@property (nullable, nonatomic, retain) NSString *desc;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) NSNumber *unique;
@property (nullable, nonatomic, retain) NSString *vider;
@property (nullable, nonatomic, retain) City *city;
@property (nullable, nonatomic, retain) NSSet<ArtistImage *> *image;

@end

@interface Artist (CoreDataGeneratedAccessors)

- (void)addImageObject:(ArtistImage *)value;
- (void)removeImageObject:(ArtistImage *)value;
- (void)addImage:(NSSet<ArtistImage *> *)values;
- (void)removeImage:(NSSet<ArtistImage *> *)values;

@end

NS_ASSUME_NONNULL_END
