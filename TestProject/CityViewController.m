//
//  CityViewController.m
//  TestProject
//
//  Created by Oleg on 1/15/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "CityViewController.h"
#import "CustomUI.h"
#import "Country.h"
#import "CityCell.h"
#import "Helper.h"
#import <MagicalRecord/MagicalRecord.h>

@interface CityViewController ()

@property (nonatomic, strong) NSArray *cities;

@end

@implementation CityViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    int height = self.navigationController.navigationBar.frame.size.height;
    int width = self.navigationController.navigationBar.frame.size.width;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.titleView = [CustomUI getCustomNavigationLabel:@"Города" withSize:CGSizeMake(width, height) andColor:[UIColor blackColor] align:NSTextAlignmentCenter];
    
    UIButton *rightButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
    [rightButton setFrame:CGRectMake(0, 0, 40, 40)];
    [rightButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    self.navigationItem.rightBarButtonItem = [CustomUI getCustomRightButton];
    
    //cities
    NSString *cityID = [[NSUserDefaults standardUserDefaults] objectForKey:SelectedCity];
    if (cityID && cityID.length > 0) {
        City *selectedCity = [City MR_findFirstByAttribute:@"unique" withValue:cityID inContext:ManagedObjectContext];
        if (selectedCity) {
            Country *country = selectedCity.country;
            _cities = [country.city allObjects];
            [self.tableView reloadData];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cities count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell" forIndexPath:indexPath];
    
    // Configure the cell...
    City *city = [self.cities objectAtIndex:indexPath.row];
    cell.city = city;
    [cell configureCell];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


@end
