//
//  ArtistCell.h
//  TestProject
//
//  Created by Oleg on 1/15/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Artist.h"

@interface ArtistCell : UITableViewCell

@property (nonatomic, strong) Artist *artist;
@property (weak, nonatomic) IBOutlet UIImageView *artistImageView;
@property (weak, nonatomic) IBOutlet UILabel *artistNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistDecriptionLabel;

-(void)configureCell;

@end
