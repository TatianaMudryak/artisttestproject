//
//  ArtistImage+Create.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "ArtistImage+Create.h"
#import "Helper.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation ArtistImage (Create)

+(ArtistImage *)createCityWithDict:(NSDictionary *)dict artist:(Artist *)artist
{
    ArtistImage *image = [ArtistImage MR_createEntityInContext:ManagedObjectContext];
    image.imageLink = [dict[@"ImageLink"] isKindOfClass:[NSNull class]] ? @"" : dict[@"ImageLink"];
    image.watermark = [dict[@"ShouldShowWatermark"] isKindOfClass:[NSNull class]] ? NO : dict[@"ShouldShowWatermark"];;
    image.artist = artist;
    
    [ManagedObjectContext MR_saveToPersistentStoreAndWait];
    return image;
}

@end
