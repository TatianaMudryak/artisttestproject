//
//  Artist+CoreDataProperties.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Artist+CoreDataProperties.h"

@implementation Artist (CoreDataProperties)

@dynamic age;
@dynamic desc;
@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic phone;
@dynamic unique;
@dynamic vider;
@dynamic city;
@dynamic image;

@end
