//
//  LaunchScreenViewController.m
//  TestProject
//
//  Created by Oleg on 1/13/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "LaunchScreenViewController.h"
#import "Helper.h"

@interface LaunchScreenViewController ()

@end

@implementation LaunchScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;

    
    [self performSelector:@selector(showStartScreen) withObject:nil afterDelay:2.0];
}

- (void)showStartScreen
{
    //get selected city
    NSString *cityID = [[NSUserDefaults standardUserDefaults] objectForKey:SelectedCity];
    if (cityID && cityID.length > 0)
        [self performSegueWithIdentifier:@"showSelectedCity" sender:self];
    else
        [self performSegueWithIdentifier:@"showStartScreen" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





@end
