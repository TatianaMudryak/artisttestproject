//
//  CountryCell.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "CityCell.h"

@implementation CityCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell
{
    self.cityNameLabel.text = self.city.name;
    self.cityImageView.image = [UIImage imageWithData:self.city.imageData];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.cityImageView.layer.cornerRadius = self.cityImageView.frame.size.width/2;
    self.cityImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.cityImageView.layer.masksToBounds = YES;
    self.cityImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.cityImageView.layer.borderWidth = 2.0;
}

@end
