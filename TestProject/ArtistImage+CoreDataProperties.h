//
//  ArtistImage+CoreDataProperties.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ArtistImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArtistImage (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *imageLink;
@property (nullable, nonatomic, retain) NSNumber *watermark;
@property (nullable, nonatomic, retain) NSData *imageData;
@property (nullable, nonatomic, retain) Artist *artist;

@end

NS_ASSUME_NONNULL_END
