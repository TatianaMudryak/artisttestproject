//
//  ArtistCell.m
//  TestProject
//
//  Created by Oleg on 1/15/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "ArtistCell.h"

@implementation ArtistCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)configureCell
{
    self.artistNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.artist.firstName, self.artist.lastName];
    self.artistDecriptionLabel.text = self.artist.desc;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.artistImageView.layer.cornerRadius = self.artistImageView.frame.size.width/2;
    self.artistImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.artistImageView.layer.masksToBounds = YES;
    self.artistImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.artistImageView.layer.borderWidth = 2.0;
}

@end
