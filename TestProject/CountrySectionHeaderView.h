//
//  CountrySectionHeaderView.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountrySectionHeaderViewDelegate;

@interface CountrySectionHeaderView : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property (weak, nonatomic) IBOutlet UILabel *countryNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *disclosureButton;
@property (nonatomic) NSInteger section;
@property (nonatomic, weak) id <CountrySectionHeaderViewDelegate> delegate;
@end

@protocol CountrySectionHeaderViewDelegate <NSObject>

@optional
- (void)sectionHeaderView:(CountrySectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)section;
- (void)sectionHeaderView:(CountrySectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)section;

@end