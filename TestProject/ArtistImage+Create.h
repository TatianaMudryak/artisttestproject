//
//  ArtistImage+Create.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "ArtistImage.h"

@interface ArtistImage (Create)

+(ArtistImage *)createCityWithDict:(NSDictionary *)dict artist:(Artist *)artist;

@end
