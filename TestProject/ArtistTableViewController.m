//
//  ArtistTableViewController.m
//  TestProject
//
//  Created by Oleg on 1/15/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "ArtistTableViewController.h"
#import "CustomUI.h"
#import "Helper.h"
#import "City.h"
#import "Artist.h"
#import "ArtistImage.h"
#import "ArtistCell.h"
#import <MagicalRecord/MagicalRecord.h>

@interface ArtistTableViewController ()

@property (nonatomic, strong) NSArray *artists;

@end

@implementation ArtistTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *cityID = [[NSUserDefaults standardUserDefaults] objectForKey:SelectedCity];
    if (cityID && cityID.length > 0) {
        City *selectedCity = [City MR_findFirstByAttribute:@"unique" withValue:cityID inContext:ManagedObjectContext];
        if (selectedCity) {
            _artists = [selectedCity.artist allObjects];
            [self.tableView reloadData];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.artists count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ArtistCell *cell = [tableView dequeueReusableCellWithIdentifier:@"artistCell" forIndexPath:indexPath];
    
    Artist *artist = [self.artists objectAtIndex:indexPath.row];
    cell.artist = artist;
    if ([artist.image allObjects].count > 0) {
        ArtistImage *artistImage = [artist.image allObjects].firstObject;
        if (!artistImage.imageData && artistImage.imageLink)
            [self downloadImageForAertist:artistImage];
        cell.artistImageView.image = [UIImage imageWithData:artistImage.imageData];
    }
    
    [cell configureCell];
    return cell;
}

-(void)downloadImageForAertist:(ArtistImage *)image
{
    NSURL *url = [NSURL URLWithString:image.imageLink];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            image.imageData = data;
            [ManagedObjectContext MR_saveToPersistentStoreAndWait];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    }];
    
    [task resume];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

@end
