//
//  Artist+Create.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "Artist.h"

@interface Artist (Create)

+(Artist *)createCountryWithDict:(NSDictionary *)dict city:(City *)city;

@end
