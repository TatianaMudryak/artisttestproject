//
//  CountrySectionHeaderView.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "CountrySectionHeaderView.h"

@interface CountrySectionHeaderView ()

@property (nonatomic) BOOL open;

@end

@implementation CountrySectionHeaderView

- (void)drawRect:(CGRect)rect
{
    self.countryImageView.layer.cornerRadius = self.countryImageView.frame.size.width/2;
    self.countryImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.countryImageView.layer.masksToBounds = YES;
    self.countryImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.countryImageView.layer.borderWidth = 2.0;
}

- (void)awakeFromNib
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(openSection)];
    [self addGestureRecognizer:tapGesture];
}


- (void)openSection
{
    self.disclosureButton.selected = !self.disclosureButton.selected;
    
    _open = !_open;
    
    if (_open) {
        if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)])
            [self.delegate sectionHeaderView:self sectionOpened:self.section];
        
    }
    else {
        if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)])
            [self.delegate sectionHeaderView:self sectionClosed:self.section];
        
    }

}

@end
