//
//  ServerAPIWrapper+City.h
//  TestProject
//
//  Created by Oleg on 1/13/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "ServerAPIWrapper.h"

@interface ServerAPIWrapper (City)

-(void)getCityList;

@end
