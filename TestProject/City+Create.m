//
//  City+Create.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "City+Create.h"
#import "Helper.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation City (Create)

+(City *)createCityWithDict:(NSDictionary *)dict country:(Country *)country
{
    City *newCity = [City MR_createEntityInContext:ManagedObjectContext];
    newCity.name = dict[@"Name"];
    newCity.unique = dict[@"Id"];
    newCity.imageLink = [dict[@"ImageLink"] isKindOfClass:[NSNull class]] ? @"" : dict[@"ImageLink"];
    newCity.country = country;
    
    [ManagedObjectContext MR_saveToPersistentStoreAndWait];
    return newCity;
}


@end
