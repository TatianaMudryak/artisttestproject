//
//  CustomUI.h
//  ParkingFix
//
//  Created by Tanya on 2/21/14.
//  Copyright (c) 2014 Mudryak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CustomUI : NSObject

+(UIImage *)getCustomNavigationBarWithColor:(UIColor *)barColor;
+(UILabel *)getCustomNavigationLabel:(NSString *)text withSize:(CGSize)size andColor:(UIColor *)labelColor align:(NSTextAlignment)align;
+(UIBarButtonItem *)getCustomRightButton;
@end
