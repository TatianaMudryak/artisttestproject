//
//  DetailViewController.m
//  TestProject
//
//  Created by Oleg on 1/15/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "DetailViewController.h"
#import "CustomUI.h"
#import "LaunchScreenViewController.h"

@interface DetailViewController () <UITabBarControllerDelegate>

@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
  
    [self setPageTitle:@"Артисты"];
    
    UIButton *rightButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
    [rightButton setFrame:CGRectMake(0, 0, 40, 40)];
    [rightButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];

    self.navigationItem.rightBarButtonItem = [CustomUI getCustomRightButton];
}

-(void)goBack
{
    NSInteger myIndex = [self.navigationController.viewControllers indexOfObject:self];
    
    if ([[self.navigationController.viewControllers objectAtIndex:myIndex-1] isKindOfClass:[LaunchScreenViewController class]])
        [self performSegueWithIdentifier:@"chooseCity" sender:self];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    [self setPageTitle:item.title];
}

-(void)setPageTitle:(NSString *)title
{
    int height = self.navigationController.navigationBar.frame.size.height;
    int width = self.navigationController.navigationBar.frame.size.width;
    self.navigationItem.titleView = [CustomUI getCustomNavigationLabel:title withSize:CGSizeMake(width, height) andColor:[UIColor blackColor] align:NSTextAlignmentCenter];
}

@end
