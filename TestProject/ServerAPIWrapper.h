//
//  ServerAPIWrapper.h
//  TestProject
//
//  Created by Oleg on 1/13/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ServerApiWrapperDelegate <NSObject>

-(void)searchResult:(BOOL)success error:(NSString *)error;


@end

@interface ServerAPIWrapper : NSObject

@property (nonatomic, assign) id <ServerApiWrapperDelegate> delegate;

+(ServerAPIWrapper *)sharedServerWrapper;



@end
