//
//  CountryViewController.m
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "CountryViewController.h"
#import "CustomUI.h"
#import "Country.h"
#import "CityCell.h"
#import "Helper.h"
#import <MagicalRecord/MagicalRecord.h>
#import "CountrySectionInfo.h"
#import "CountrySectionHeaderView.h"
#import "DetailViewController.h"
#import "ArtistTableViewController.h"
#import "CityViewController.h"

@interface CountryViewController () <CountrySectionHeaderViewDelegate>

@property (nonatomic) NSMutableArray *sectionInfoArray;
@property (nonatomic) NSInteger openSectionIndex;

@end

@implementation CountryViewController

static NSString *SectionHeaderViewIdentifier = @"SectionHeaderViewIdentifier";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    self.tableView.sectionHeaderHeight = 80.0;
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"CountrySectionHeaderView" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:SectionHeaderViewIdentifier];

    
    int height = self.navigationController.navigationBar.frame.size.height;
    int width = self.navigationController.navigationBar.frame.size.width;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.titleView = [CustomUI getCustomNavigationLabel:@"Выбор страны" withSize:CGSizeMake(width, height) andColor:[UIColor blackColor] align:NSTextAlignmentCenter];
    
    UIButton *rightButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
    [rightButton setFrame:CGRectMake(0, 0, 40, 40)];
    [rightButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    self.navigationItem.rightBarButtonItem = [CustomUI getCustomRightButton];
    
    //fill info about each section
    if ((self.sectionInfoArray == nil) ||
        (self.sectionInfoArray.count != [self numberOfSectionsInTableView:self.tableView])) {
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        for (Country *country in self.countries) {
            
            CountrySectionInfo *sectionInfo = [[CountrySectionInfo alloc] init];
            sectionInfo.country = country;
            sectionInfo.open = NO;
            
            [infoArray addObject:sectionInfo];
        }
        
        self.sectionInfoArray = infoArray;
    }

}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.countries.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CountrySectionInfo *sectionInfo = (self.sectionInfoArray)[section];
    NSInteger rowsInSection = [[sectionInfo.country.city allObjects] count];
    
    return sectionInfo.open ? rowsInSection : 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell" forIndexPath:indexPath];
    
    Country *country = (Country *)[(self.sectionInfoArray)[indexPath.section] country];
    City *city = [country.city allObjects][indexPath.row];
    cell.city = city;
    if (!city.imageData && city.imageLink)
        [self downloadImageForCity:city];
    [cell configureCell];
    
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CountrySectionHeaderView *sectionHeaderView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:SectionHeaderViewIdentifier];
    
    CountrySectionInfo *sectionInfo = (self.sectionInfoArray)[section];
    sectionInfo.headerView = sectionHeaderView;
    
    sectionHeaderView.countryNameLabel.text = sectionInfo.country.name;
    sectionHeaderView.section = section;
    
    if (sectionInfo.country.imageLink && !sectionInfo.country.imageData)
        [self downloadImageForCountry:sectionInfo.country];
    else
        sectionHeaderView.countryImageView.image = [UIImage imageWithData:sectionInfo.country.imageData];
    sectionHeaderView.delegate = self;
    
    return sectionHeaderView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    City *selectedCity = cell.city;
    [[NSUserDefaults standardUserDefaults] setObject:[selectedCity.unique stringValue] forKey:SelectedCity];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Download images

-(void)downloadImageForCountry:(Country *)country
{
    NSURL *url = [NSURL URLWithString:country.imageLink];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            country.imageData = data;
            [ManagedObjectContext MR_saveToPersistentStoreAndWait];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    }];
    
    [task resume];
}

-(void)downloadImageForCity:(City *)city
{
    NSURL *url = [NSURL URLWithString:city.imageLink];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            city.imageData = data;
            [ManagedObjectContext MR_saveToPersistentStoreAndWait];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
    }];
    
    [task resume];
}

#pragma mark - CountrySectionHeaderView Delegate

- (void)sectionHeaderView:(CountrySectionInfo *)sectionHeaderView sectionOpened:(NSInteger)sectionOpened
{
    CountrySectionInfo *sectionInfo = (self.sectionInfoArray)[sectionOpened];
    
    sectionInfo.open = YES;
    
    //Create an array of the index paths
    NSInteger newRowsCount = [[sectionInfo.country.city allObjects] count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < newRowsCount; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    // apply the updates
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
    self.openSectionIndex = sectionOpened;
}

- (void)sectionHeaderView:(CountrySectionInfo *)sectionHeaderView sectionClosed:(NSInteger)sectionClosed
{
    CountrySectionInfo *sectionInfo = (self.sectionInfoArray)[sectionClosed];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationNone];
    }
    self.openSectionIndex = NSNotFound;
}



@end
