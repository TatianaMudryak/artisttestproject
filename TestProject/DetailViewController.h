//
//  DetailViewController.h
//  TestProject
//
//  Created by Oleg on 1/15/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UITabBarController

@property (nonatomic, strong) NSString *cityName;

@end
