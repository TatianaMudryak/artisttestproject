//
//  CustomUI.m
//  ParkingFix
//
//  Created by Tanya on 2/21/14.
//  Copyright (c) 2014 Mudryak. All rights reserved.
//

#import "CustomUI.h"
#import "Helper.h"

@implementation CustomUI

+(UIImage *)getCustomNavigationBarWithColor:(UIColor *)barColor
{
	UIGraphicsBeginImageContext(CGSizeMake(320, 64));
	UIColor *color = [UIColor whiteColor];
	[color setFill];
	UIRectFill(CGRectMake(0, 20, 320, 20));
    color = barColor;
	[color setFill];
	UIRectFill(CGRectMake(0, 20, 320, 64));
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
    return image;
}

+(UILabel *)getCustomNavigationLabel:(NSString *)text withSize:(CGSize)size andColor:(UIColor *)labelColor align:(NSTextAlignment)align
{
    int height = size.height;
	int width = size.width;
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
	navLabel.backgroundColor = [UIColor clearColor];
	navLabel.textColor = labelColor;
	navLabel.text = text;
	[navLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
	navLabel.textAlignment = align;
	return navLabel;
}


+(UIBarButtonItem *)getCustomRightButton
{
  UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
  [rightBtn setBackgroundColor:[UIColor clearColor]];
  rightBtn.frame = CGRectMake(0, 0, 30, 30);
  UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
  return rightButton;
}


@end
