//
//  StartViewController.m
//  TestProject
//
//  Created by Oleg on 1/13/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "StartViewController.h"
#import "ServerAPIWrapper+City.h"
#import "Country.h"
#import "Helper.h"
#import <MagicalRecord/MagicalRecord.h>
#import "CountryViewController.h"

@interface StartViewController () <ServerApiWrapperDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation StartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)chooseCity
{
    [_activityIndicator startAnimating];
    [ServerAPIWrapper sharedServerWrapper].delegate = self;
    [[ServerAPIWrapper sharedServerWrapper] getCityList];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showCountries"] &&
        [segue.destinationViewController isKindOfClass:[CountryViewController class]]) {
        NSArray *countries = [Country MR_findAllInContext:ManagedObjectContext];
        CountryViewController *cvc = (CountryViewController *)segue.destinationViewController;
        cvc.countries = countries;
    }
}

#pragma mark - ServerAPIWrapperDelegate

-(void)searchResult:(BOOL)success error:(NSString *)error
{
    [_activityIndicator stopAnimating];
    if (error) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ошибка!" message:error preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *button = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * _Nonnull action) {
                                        
                                    }];
        
        [alert addAction:button];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        [self performSegueWithIdentifier:@"showCountries" sender:self];
    }
}

@end
