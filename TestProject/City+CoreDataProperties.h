//
//  City+CoreDataProperties.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "City.h"

NS_ASSUME_NONNULL_BEGIN

@interface City (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *imageLink;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *unique;
@property (nullable, nonatomic, retain) NSData *imageData;
@property (nullable, nonatomic, retain) NSSet<Artist *> *artist;
@property (nullable, nonatomic, retain) Country *country;

@end

@interface City (CoreDataGeneratedAccessors)

- (void)addArtistObject:(Artist *)value;
- (void)removeArtistObject:(Artist *)value;
- (void)addArtist:(NSSet<Artist *> *)values;
- (void)removeArtist:(NSSet<Artist *> *)values;

@end

NS_ASSUME_NONNULL_END
