//
//  ServerAPI.h
//  TestProject
//
//  Created by Oleg on 1/13/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CityListCompletionHandler) (NSArray *result, NSString *error);

@interface ServerAPI : NSObject <NSURLSessionDataDelegate>

-(void)getCityListWithCompletionHandler:(CityListCompletionHandler)completionHandler;

@end
