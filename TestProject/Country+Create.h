//
//  Country+Create.h
//  TestProject
//
//  Created by Oleg on 1/14/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "Country.h"

@interface Country (Create)

+(Country *)createCountryWithDict:(NSDictionary *)dict;

@end
