//
//  ServerAPIWrapper+City.m
//  TestProject
//
//  Created by Oleg on 1/13/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "ServerAPIWrapper+City.h"
#import "ServerAPI.h"
#import "Country+Create.h"
#import "City+Create.h"
#import "Artist+Create.h"
#import "ArtistImage+Create.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Helper.h"

@implementation ServerAPIWrapper (City)

-(void)getCityList
{
    [[[ServerAPI alloc] init] getCityListWithCompletionHandler:^(NSArray *result, NSString *error) {
        if (result) {
            
            for (NSDictionary *dict in result) {
                
                Country *country = [Country MR_findFirstByAttribute:@"unique" withValue:dict[@"Id"] inContext:ManagedObjectContext];
                if (!country)
                    country = [Country createCountryWithDict:dict];
                
                if (country) {
                    NSArray *cities = dict[@"Cities"];
                    for (NSDictionary *dict in cities) {
                        
                        City *city = [City MR_findFirstByAttribute:@"unique" withValue:dict[@"Id"] inContext:ManagedObjectContext];
                        if (!city)
                            city = [City createCityWithDict:dict country:country];
                    
                        if (city) {
                            NSArray *artists = dict[@"Artists"];
                            for (NSDictionary *dict in artists) {
                                Artist *artist = [Artist MR_findFirstByAttribute:@"unique" withValue:dict[@"Id"] inContext:ManagedObjectContext];
                                if (!artist)
                                    artist = [Artist createCountryWithDict:dict city:city];
                                if (artist) {
                                    NSArray *images = dict[@"Images"];
                                    for (NSDictionary *dict in images) {
                                        ArtistImage *image = [ArtistImage MR_findFirstByAttribute:@"imageLink" withValue:dict[@"ImageLink"] inContext:ManagedObjectContext];
                                        if (!image)
                                            [ArtistImage createCityWithDict:dict artist:artist];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate searchResult:YES error:nil];
            });
        } else
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate searchResult:NO error:error];
            });
            
    }];
}

@end
