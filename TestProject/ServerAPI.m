//
//  ServerAPI.m
//  TestProject
//
//  Created by Oleg on 1/13/16.
//  Copyright © 2016 TatyanaMudryak. All rights reserved.
//

#import "ServerAPI.h"

#define HOST @"http://atw-api.azurewebsites.net/api/countries"

@implementation ServerAPI 

-(void)getCityListWithCompletionHandler:(CityListCompletionHandler)completionHandler
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:HOST]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:60.0];
    [request addValue:@"text/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSString *errorMessage = result[@"Error"];
            if ([errorMessage isKindOfClass:[NSNull class]] && result[@"Result"]) {
                NSArray *artists = result[@"Result"];
                completionHandler(artists, nil);
            }
        } else
            completionHandler(nil, error.localizedDescription);

    }];
    
    [postDataTask resume];

}


@end
